# A Learning Resource for PHP Unit Test Application

A learning resource for practicing PHP Unit using CakePHP 3

## Installation

1. Clone this repository to your vagrant/web folder.
2. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
3. Go to repository folder then Run `composer install`.

## Requirements

1. Create a 2 database  `php_unit_db` and `test_php_unit_db`
2. Copy config/app.default.php to config/app.php
3. Run initial migration to create database table by `php bin/cake.php migrations migrate`

## Running a PHP Unit

1. Go to your project folder by Running this code `cd /var/www/cakephp_unit_test_for_qa`
2. Execute a unit test by Running this code `vendor/bin/phpunit tests/TestCase/Controller/Employees/addEmployeeTest.php`

## Location of Quizzes
1. project_folder/src/Quiz.txt

## Create Initial Branch By Employee
1. Pull latest branch of master `git pull origin master`
2. Create branch from master `git checkout -b employeeNo_employeeName`
3. Push your initial branch by `git push origin employeeNo_employeeName`

## How to Submit Quiz
1. Check out to your initial branch by `git checkout employeeNo_employeeName`
2. Create branch from your branch `git checkout -b employeeNo_employeeName_quizNo`
3. Push your quiz branch by `git push origin employeeNo_employeeName_quizNo`
4. Submit Pull Request :)